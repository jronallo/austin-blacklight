module ApplicationHelper

  def facet_value_null(value)
    value ? value : 'null'
  end

end
