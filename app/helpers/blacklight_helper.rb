module BlacklightHelper
  include Blacklight::BlacklightHelperBehavior

  def application_name
    'Austin Blacklight'
  end
end