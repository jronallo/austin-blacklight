# README

This is a [Blacklight](https://github.com/projectblacklight/blacklight) application for exploring data created with the [austin Rubygem](https://bitbucket.org/jronallo/austin). In order for this to work you can simply copy over the solr config in doc/solr_config/ to the example Solr setup (into solr-4.4.0/example/solr/collection1/conf/).

See the austin project for information on how to load CSV data from the Web Data Commons into Solr.

Because of the size of the dataset you may want to start Solr with more memory allocated:

```
 java -Xmx1024m  -jar start.jar
```

## Facets

Most facets are named after whether they were extracted from the subject, predicate, object, or context of the NQuad. The schema.org facets all come from the predicate.